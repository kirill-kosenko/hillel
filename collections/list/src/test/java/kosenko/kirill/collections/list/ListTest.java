package kosenko.kirill.collections.list;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public abstract class ListTest {

    protected List<Integer> list;

    @Test
    public void sizeTest() {
        assertEquals(6, list.size());
    }

    @Test
    public void getTest() {
        assertEquals(6, list.size());
        for (int i = 0; i < list.size(); i++) {
            assertEquals(new Integer(i + 1), list.get(i));
        }
    }

    @Test
    public void addTest() {
        list.add(3);
        assertEquals(new Integer(3), list.get(6));
    }

    @Test
    public void addByIndexTest() {
        list.add(2, 13);

        assertEquals(7, list.size());
        assertEquals(new Integer(13), list.get(2));
    }

    @Test
    public void setTest() {
        Integer e = list.set(3, 10);

        assertEquals(6, list.size());
        assertEquals(new Integer(4), e);
        assertEquals(new Integer(10), list.get(3));
    }

    @Test
    public void indexOfTest() {
        int expected = 4;
        int actual = list.indexOf(5);

        assertEquals(expected, actual);
    }

    @Test
    public void lastIndexOfTest() {
        int expected = 4;
        list.add(expected, 2);

        int actual = list.lastIndexOf(2);

        assertEquals(expected, actual);
    }

    @Test
    public void removeTest() {
        Integer e = list.remove(2);

        assertEquals(5, list.size());
        assertEquals(new Integer(3), e);
        assertNotEquals(new Integer(3), list.get(2));
    }
}
