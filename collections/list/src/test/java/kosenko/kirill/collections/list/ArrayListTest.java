package kosenko.kirill.collections.list;

import org.junit.Before;

public class ArrayListTest extends ListTest {

    @Before
    public void before() {
        list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
    }

}
