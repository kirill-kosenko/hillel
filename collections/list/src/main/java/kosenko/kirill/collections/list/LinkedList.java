package kosenko.kirill.collections.list;

import java.util.NoSuchElementException;

public class LinkedList<T> implements List<T> {

    protected int size;
    protected Node<T> first;
    protected Node<T> last;

    @Override
    public int size() {
        return size;
    }

    @Override
    public void add(T element) {
        insertLast(element);
    }

    @Override
    public void add(int index, T element) {
        checkIndex(index);
        Node<T> node = getNode(index);
        insertBefore(node, element);
    }

    protected void insertLast(T element) {
        Node<T> l = last;
        Node<T> node = new Node<>(l, element, null);
        last = node;
        if (l == null) {
            first = node;
        } else {
            l.next = node;
        }
        size++;
    }

    protected void insertFirst(T element) {
        Node<T> f = first;
        Node<T> newNode = new Node<>(null, element, f);
        first = newNode;
        if (f == null) {
            last = newNode;
        } else {
            f.prev = newNode;
        }
        size++;
    }

    private void insertBefore(Node<T> node, T element) {
        linkBefore(element, node);
    }

    @Override
    public T set(int index, T element) {
        checkIndex(index);

        Node<T> node = getNode(index);
        T old = node.data;
        node.data = element;

        return old;
    }

    @Override
    public T get(int index) {
        return getNode(index).data;
    }

    @Override
    public int indexOf(T element) {
        Node<T> node = first;
        T current = null;
        for (int i = 0; i < size; i++) {
            current = node.data;
            if (current.equals(element))
                return i;
            node = node.next;
        }
        return -1;
    }

    @Override
    public int lastIndexOf(T element) {
        Node<T> node = last;
        T current = null;
        for (int i = size - 1; i >= 0; i--) {
            current = node.data;
            if (current.equals(element))
                return i;
            node = node.prev;
        }
        return -1;
    }

    @Override
    public T remove(int index) {
        checkIndex(index);
        Node<T> node = getNode(index);
        return unlink(node);
    }

    protected T unlinkFirst() {
        if (first == null) {
            throw new NoSuchElementException();
        }
        T data = first.data;
        Node<T> next = first.next;
        first = next;
        if (next == null)
            last = null;
        else
            next.prev = null;
        size--;
        return data;
    }

    protected void linkBefore(T data, Node<T> node) {
        Node<T> prev = node.prev;
        Node<T> newNode = new Node<>(prev, data, node);
        node.prev = newNode;
        if (prev == null)
            first = newNode;
        else
            prev.next = newNode;
        size++;
    }

    protected T unlink(Node<T> node) {
        T data = node.data;
        Node<T> prev = node.prev;
        Node<T> next = node.next;

        if (prev == null) {
            first = next;
        } else {
            prev.next = next;
        }

        if (next == null) {
            last = prev;
        } else {
            next.prev = prev;
        }

        node = null;
        size--;

        return data;
    }

    protected Node<T> getNode(int index) {
        Node<T> current = null;
        if (index < (size >> 1)) {
            current = first;
            for (int i = 0; i < index; i++) {
                current = current.next;
            }
        } else {
            current = last;
            for (int i = size - 1; i > index; i--) {
                current = current.prev;
            }
        }
        return current;
    }

    private void checkIndex(int index) {
        if (size <= index) {
            throw new IndexOutOfBoundsException();
        }
    }

    protected static class Node<T> {
        protected Node<T> prev;
        protected Node<T> next;
        protected T data;

        public Node(Node<T> prev, T data, Node<T> next) {
            this.prev = prev;
            this.next = next;
            this.data = data;
        }
    }
}
