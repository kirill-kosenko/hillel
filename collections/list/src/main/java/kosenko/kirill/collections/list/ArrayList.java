package kosenko.kirill.collections.list;

public class ArrayList<T> implements List<T> {

    private T[] data;
    private int size;

    private static final int DEFAULT_CAPCITY = 10;

    public ArrayList() {
        data = newArray(DEFAULT_CAPCITY);
    }

    public ArrayList(int capacity) {
        data = newArray(capacity);
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void add(T element) {
        ensureCapacity(size + 1);
        data[size++] = element;
    }

    @Override
    public void add(int index, T element) {
        ensureCapacity(size + 1);
        System.arraycopy(data, index, data, index + 1, size - index);
        data[index] = element;
        size++;
    }

    @Override
    public T set(int index, T element) {
        checkIndex(index);

        T old = data[index];
        data[index] = element;
        return old;
    }

    @Override
    public T get(int index) {
        return data[index];
    }

    @Override
    public int indexOf(T element) {
        for (int i = 1; i < size; i++) {
            if (data[i].equals(element))
                return i;
        }
        return -1;
    }

    @Override
    public int lastIndexOf(T element) {
        for (int i = size-1; i >= 0; i--) {
            if (data[i].equals(element)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public T remove(int index) {
        checkIndex(index);

        T removed = data[index];
        int length = size - index - 1;
        if (length > 0)
            System.arraycopy(data, index+1, data, index, length);

        data[--size] = null;
        return removed;
    }

    private void ensureCapacity(int capacity) {
        if (data.length < capacity) {
            increase(capacity);
        }
    }

    private void increase(int capacity) {
        int oldCapacity = data.length;
        int newCapacity = ((oldCapacity*3) >> 1) + 1;
        if (newCapacity < capacity) {
            newCapacity = capacity;
        }
        data = copy(newCapacity);
    }

    private T[] copy(int capacity) {
        T[] dest = newArray(capacity);
        System.arraycopy(data, 0, dest, 0, data.length);
        return dest;
    }

    private void checkIndex(int index) {
        if (size <= index)
            throw new IndexOutOfBoundsException();
    }

    @SuppressWarnings("unchecked")
    private T[] newArray(int capacity) {
        return (T[]) new Object[capacity];
    }
}
