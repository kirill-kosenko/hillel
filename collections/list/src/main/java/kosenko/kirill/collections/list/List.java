package kosenko.kirill.collections.list;

public interface List<T> {

    void add(T element);

    void add(int index, T element);

    T set(int index, T element);

    T get(int index);

    int indexOf(T element);

    int lastIndexOf(T element);

    T remove(int index);

    int size();
}
