package kosenko.kirill.collections.queue;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class LinkedStackTest {

    private LinkedStack<Integer> stack;

    @Before
    public void before() {
        stack = new LinkedStack<Integer>() {
            {
                add(5);
                add(10);
                add(31);
                add(23);
                add(1);
                add(65);
                add(100);
                add(123);
                add(567);
                add(1);
            }
        };

    }

    @Test
    public void popTest() {
        Assert.assertEquals(new Integer(1), stack.pop());
        Assert.assertEquals(new Integer(567), stack.pop());
        Assert.assertEquals(new Integer(123), stack.pop());
        Assert.assertEquals(new Integer(100), stack.pop());
        Assert.assertEquals(new Integer(65), stack.pop());
        Assert.assertEquals(new Integer(1), stack.pop());
        Assert.assertEquals(new Integer(23), stack.pop());
        Assert.assertEquals(new Integer(31), stack.pop());
        Assert.assertEquals(new Integer(10), stack.pop());
        Assert.assertEquals(new Integer(5), stack.pop());
    }

    @Test
    public void pushTest() {
        LinkedStack<Integer> stack = new LinkedStack<>();
        stack.push(1);
        stack.push(5);
        stack.push(10);
        stack.push(2);
        stack.push(999);

        Assert.assertEquals(5, stack.size());
        Assert.assertEquals(new Integer(999), stack.poll());
        Assert.assertEquals(new Integer(2), stack.poll());
        Assert.assertEquals(new Integer(10), stack.poll());
        Assert.assertEquals(new Integer(5), stack.poll());
        Assert.assertEquals(new Integer(1), stack.poll());

    }

    @Test
    public void peekNullTest() {
        LinkedStack<Integer> stack = new LinkedStack<>();
        Assert.assertNull(stack.peek());
    }

    @Test
    public void peekTest() {
        Assert.assertEquals(new Integer(1), stack.peek());
        stack.poll();
        Assert.assertEquals(new Integer(567), stack.peek());
    }
}
