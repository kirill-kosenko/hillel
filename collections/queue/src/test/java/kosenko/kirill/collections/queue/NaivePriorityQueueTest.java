package kosenko.kirill.collections.queue;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class NaivePriorityQueueTest {

    private NaivePriorityQueue<Integer> queue;

    @Before
    public void before() {
        queue = new NaivePriorityQueue<Integer>() {{
            add(5);
            add(10);
            add(31);
            add(23);
            add(1);
            add(65);
            add(100);
            add(123);
            add(567);
            add(1);
        }
        };

    }

    @Test
    public void sizeTest() {
        Assert.assertEquals(10, queue.size());
    }

    @Test
    public void pollTest() {
        Assert.assertEquals(new Integer(1), queue.poll());
        Assert.assertEquals(new Integer(1), queue.poll());
        Assert.assertEquals(new Integer(5), queue.poll());
        Assert.assertEquals(new Integer(10), queue.poll());
        Assert.assertEquals(new Integer(23), queue.poll());
        Assert.assertEquals(new Integer(31), queue.poll());
        Assert.assertEquals(new Integer(65), queue.poll());
        Assert.assertEquals(new Integer(100), queue.poll());
        Assert.assertEquals(new Integer(123), queue.poll());
        Assert.assertEquals(new Integer(567), queue.poll());
    }

    @Test
    public void addTest() {
        Queue<Integer> queue = new NaivePriorityQueue<>();
        queue.add(6);
        queue.add(2);
        queue.add(32);
        queue.add(0);
        queue.add(21);

        Assert.assertEquals(new Integer(0), queue.poll());
        Assert.assertEquals(new Integer(2), queue.poll());
        Assert.assertEquals(new Integer(6), queue.poll());
        Assert.assertEquals(new Integer(21), queue.poll());
        Assert.assertEquals(new Integer(32), queue.poll());
    }

    @Test
    public void peekTest() {
        Assert.assertEquals(new Integer(1), queue.peek());
        queue.poll();
        Assert.assertEquals(new Integer(1), queue.peek());
    }

}
