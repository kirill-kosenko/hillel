package kosenko.kirill.collections.queue;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class LinkedQueueTest {

    private LinkedQueue<Integer> queue;

    @Before
    public void before() {
        queue = new LinkedQueue<Integer>() {{
            add(5);
            add(10);
            add(31);
            add(23);
            add(1);
            add(65);
            add(100);
            add(123);
            add(567);
            add(1);
        }
        };

    }

    @Test
    public void sizeTest() {
        int expected = 10;
        Assert.assertEquals(expected, queue.size());
    }

    @Test
    public void pollTest() {
        Assert.assertEquals(new Integer(5), queue.poll());
        Assert.assertEquals(new Integer(10), queue.poll());
        Assert.assertEquals(new Integer(31), queue.poll());
        Assert.assertEquals(new Integer(23), queue.poll());
        Assert.assertEquals(new Integer(1), queue.poll());
        Assert.assertEquals(new Integer(65), queue.poll());
        Assert.assertEquals(new Integer(100), queue.poll());
        Assert.assertEquals(new Integer(123), queue.poll());
        Assert.assertEquals(new Integer(567), queue.poll());
        Assert.assertEquals(new Integer(1), queue.poll());
    }

    @Test
    public void addTest() {
        LinkedQueue<Integer> queue = new LinkedQueue<>();
        queue.add(1);
        queue.add(4);
        queue.add(5);
        queue.add(11);
        queue.add(7);

        int sizeExpected = 5;

        Assert.assertEquals(sizeExpected, queue.size());
        Assert.assertEquals(new Integer(1), queue.poll());
        Assert.assertEquals(new Integer(4), queue.poll());
        Assert.assertEquals(new Integer(5), queue.poll());
        Assert.assertEquals(new Integer(11), queue.poll());
        Assert.assertEquals(new Integer(7), queue.poll());
    }

    @Test
    public void peekNullTest() {
        LinkedQueue<Integer> queue = new LinkedQueue<>();
        Assert.assertNull(queue.peek());
    }

    @Test
    public void peekTest() {
        Assert.assertEquals(new Integer(5), queue.peek());
        queue.poll();
        Assert.assertEquals(new Integer(10), queue.peek());
    }



}
