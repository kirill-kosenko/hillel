package kosenko.kirill.collections.queue;

import org.junit.Assert;
import org.junit.Test;

public class NaivePriorityQueueModifiedTest {

    private Queue<Integer> queue;

    @Test
    public void addPollTest() {
        Queue<Integer> queue = new NaivePriorityQueueModified<>(3);
        queue.add(5);
        queue.add(2);
        queue.add(0);
        queue.add(3);
        queue.add(2);
        queue.add(9);
        queue.add(3);
        queue.add(7);
        queue.add(4);
        queue.add(2);

        Assert.assertEquals(10, queue.size());
        Assert.assertEquals(new Integer(0), queue.poll());
        Assert.assertEquals(new Integer(2), queue.poll());
        Assert.assertEquals(new Integer(2), queue.poll());

        Assert.assertEquals(new Integer(5), queue.poll());

        Assert.assertEquals(new Integer(2), queue.poll());
        Assert.assertEquals(new Integer(3), queue.poll());
        Assert.assertEquals(new Integer(3), queue.poll());

        Assert.assertEquals(new Integer(9), queue.poll());

        Assert.assertEquals(new Integer(4), queue.poll());
        Assert.assertEquals(new Integer(7), queue.poll());

    }

}
