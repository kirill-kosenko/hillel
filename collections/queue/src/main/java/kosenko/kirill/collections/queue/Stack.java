package kosenko.kirill.collections.queue;

public interface Stack<T> extends Queue<T> {
    void push(T element);
    T pop();
}
