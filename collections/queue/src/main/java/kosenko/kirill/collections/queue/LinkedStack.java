package kosenko.kirill.collections.queue;

public class LinkedStack<T> extends LinkedQueue<T> implements Stack<T> {

    @Override
    public void add(T element) {
        push(element);
    }

    @Override
    public void push(T element) {
        insertFirst(element);
    }

    @Override
    public T pop() {
        return poll();
    }
}
