package kosenko.kirill.collections.queue;


public class NaivePriorityQueueModified<T extends Comparable<T>> implements Queue<T> {

    private int size;

    private Queue<Wrapper<T>> inTurnQueue = new LinkedQueue<>();
    private Queue<Wrapper<T>> priorityQueue = new NaivePriorityQueue<>();

    private static final int DEFAULT_PRIORITY_COUNT = 5;
    private final int PRIORITY_COUNT;
    private int current;

    public NaivePriorityQueueModified() {
        PRIORITY_COUNT = DEFAULT_PRIORITY_COUNT;
    }

    public NaivePriorityQueueModified(int count) {
        PRIORITY_COUNT = count;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void add(T element) {
        Wrapper<T> wrapper = new Wrapper<>(element);
        priorityQueue.add(wrapper);
        inTurnQueue.add(wrapper);
        size++;
    }

    @Override
    public T poll() {
        Queue<Wrapper<T>> q;

        if (current < PRIORITY_COUNT) {
            q = priorityQueue;
            current++;
        } else {
            q = inTurnQueue;
            current = 0;
        }

        size--;
        return pollValue(q);
    }

    private T pollValue(Queue<Wrapper<T>> queue) {
        Wrapper<T> wrapper = getWrapper(queue);
        T value = wrapper.element;
        wrapper.element = null;
        return value;
    }

    @Override
    public T peek() {
        if (current < PRIORITY_COUNT) {
            return getWrapper(priorityQueue).element;
        } else {
            return getWrapper(inTurnQueue).element;
        }
    }

    private Wrapper<T> getWrapper(Queue<Wrapper<T>> queue) {
        Wrapper<T> wrapper = queue.poll();
        while (wrapper.element == null) {
            wrapper = queue.poll();
        }
        return wrapper;
    }


    private static class Wrapper<T extends Comparable<T>> implements Comparable<Wrapper<T>> {
        private T element;

        public Wrapper(T element) {
            this.element = element;
        }

        @Override
        public int compareTo(Wrapper<T> o) {
            if (o.element == null) return 1;
            return element.compareTo(o.element);
        }
    }
}
