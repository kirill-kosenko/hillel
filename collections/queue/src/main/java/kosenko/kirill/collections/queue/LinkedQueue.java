package kosenko.kirill.collections.queue;

public class LinkedQueue<T> extends LinkedList<T> implements Queue<T> {

    @Override
    public T poll() {
        T removed = first.data;
        Node<T> next = first.next;
        first = next;
        if (next == null)
            last = null;
        else
            next.prev = null;
        size--;
        return removed;
    }

    @Override
    public T peek() {
        return (first == null) ? null : first.data;
    }
}
