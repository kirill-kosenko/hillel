package kosenko.kirill.collections.queue;

public class NaivePriorityQueue<T extends Comparable<T>> extends LinkedQueue<T> implements Queue<T> {


    @Override
    public void add(T e) {
        insertUsingComparable(e);
    }

    private void insertUsingComparable(T e) {
        Node<T> c = first;
        int i;
        for (i = 0; i < size; i++) {
            if (e.compareTo(c.data) < 0)
                break;

            c = c.next;
        }
        insert(c, e);
    }

    private void insert(Node<T> node, T e) {
        if (node == null)
            insertLast(e);
        else
            insertBefore(node, e);
    }
}
