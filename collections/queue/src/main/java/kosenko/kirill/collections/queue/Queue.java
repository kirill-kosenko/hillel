package kosenko.kirill.collections.queue;

public interface Queue<T> {

    void add(T element);

    T poll();

    T peek();

    int size();

}
