package hillel.kosenko.kirill.calculation.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class RutishauserParserCalculationTest {

    private String PARSER_NAME = "Rutishauser";
    private CalculationServiceFacade service = new CalculationServiceFacadeImpl(PARSER_NAME);

    private String str;
    private Number result;

    public RutishauserParserCalculationTest(String str, Number result) {
        this.str = str;
        this.result = result;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][] {
                {"(((2+2)+(5*3))-2)", 17L},
                {"((5+5)-(3*(2+3)))", -5L},
                {"((-5)+4)", -1L},
                {"((2+5)+4)", 11L},
                {"(2+((5*10) + 1))", 53L},
                {"(0b0011011 | 0b100011111)", 0b0011011L | 0b100011111L},
                {"(2 * 0b100011111)", 2L*0b100011111L},
                {"(((5/2)*100)+(24%5))", (((5d/2d)*100l)+(24d%5d))}
        };
        return Arrays.asList(data);
    }

    @Test
    public void testCalculate() {
        Assert.assertEquals(result, service.calculate(str));
    }
}
