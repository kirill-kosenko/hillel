package hillel.kosenko.kirill.calculation.service;

import org.junit.Assert;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class BauerZamelzonCalculationTest {
    private String PARSER_NAME = "BauerZamelzon";
    private CalculationServiceFacade service = new CalculationServiceFacadeImpl(PARSER_NAME);

    private String str;
    private Number result;

    public BauerZamelzonCalculationTest(String str, Number result) {
        this.str = str;
        this.result = result;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][]{
                {"1+2*3", 7L},
                {"((3*2+(3+2)-3*(2+1-5)/(-3)*(3+1)-(50/9*9) + 3*2+4+1-2*(3.4 + 2) - 100.1))", -146.89999999999998d},
                {"-1+2*3", 5L}, {"2+3--2", 7L}, {"sin(10.5)", -0.87969575997167d},
                {"0b0110001110101 + 0b1001111010101", 0b0110001110101L + 0b1001111010101L},
                {"0b0110001110101 - 0b1001111010101", 0b0110001110101L - 0b1001111010101L},
                {"0b0110001110101 * 0b1001111010101", 0b0110001110101L * 0b1001111010101L},
                {"0b0110001110101 & 0b1001111010101", 0b0110001110101L & 0b1001111010101L},
                {"0b0110001110101 | 0b1001111010101", 0b0110001110101L | 0b1001111010101L},
                {"0b0110001110101 ^ 0b1001111010101", 0b0110001110101L ^ 0b1001111010101L},
                {"~0b0110001110101", ~0b0110001110101L}, {"50 & 10", 50L & 10L}, {"32 | 1", 32L | 1L},
                {"~40", ~40L}, {"sin(3.14/2)", Math.sin(3.14/2)}, {"cos(3.14/2)", Math.cos(3.14 / 2)},
                {"5+444.22*1.5/100 - 0b1011010*(9+9)", 5L+444.22*1.5/100d - 0b1011010L*(9l+9l)},
                {"5+cos(1)/2", 5L+Math.cos(1)/2d},
                {"0b101001111 + 0b0110000101 + sin(1)*5/2*sin(1)", 0b101001111L + 0b0110000101L + Math.sin(1) * 5L / 2L* Math.sin(1)},
                {"0b101011101 | 0b100110001 ^ 0b1111000001 & 0b101001111", 0b101011101L | 0b100110001L ^ 0b1111000001L & 0b101001111L},
                {"cos(3.14/2)", Math.cos(3.14/2d)}
        };
        return Arrays.asList(data);
    }

    @org.junit.Test
    public void testCalculate() {
        Assert.assertEquals(result, service.calculate(str));

    }
}
