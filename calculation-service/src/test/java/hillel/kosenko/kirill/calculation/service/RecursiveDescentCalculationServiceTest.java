package hillel.kosenko.kirill.calculation.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class RecursiveDescentCalculationServiceTest {

    private String str;
    private Number result;

    private String PARSER_NAME = "RecursiveDescent";
    private CalculationServiceFacade service = new CalculationServiceFacadeImpl(PARSER_NAME);

    public RecursiveDescentCalculationServiceTest(String str, Number result) {
        this.str = str;
        this.result = result;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][] {
                {"sin(10.5)", -0.87969575997167d}, {"cos(10.5)", -0.47553692799599256d},
                {"0", 0L}, {"1.0", 1d}, {"9", 9L}, {"10.0", 10d},
                {"+1", 1L}, {"-1", -1L}, {"(1)", 1L}, {"(-1)", -1L},
                {"5+40", 45L}, {"31   +  12.5", 43.5}, {"10+30+100", 140L},
                {"2+5+20+100+1000", 1127L}, {"-5+20", 15L}, {"(5+20)", 25L},
                {"-3*5", -15L}, {"5*-2", -10L}, {"10/-2", -5d},
                {"5+10*2.5", 30d}, {"50*5 - 100", 150L}, {"(5+10)*2", 30L},
                {"(2+2)*(5+5)", 40L}, {"2*2 + 5*5", 29L}, {"4 + 30/-3+100/50-10", -14d},
                {"(((2+2)+(5*3))-2)", 17L},
                {"((3*2+(3+2)-3*(2+1-5)/-3*(3+1)-(50/9*9) + 3*2+4+1-2*(3.4 + 2) - 100.1))", -146.89999999999998d},
                {"(5)+(4)", 9L}, {"2+3--2", 7L},
                {"5+cos(1)/2", 5L+Math.cos(1)/2d} ,
                {"0b101001111 + 0b0110000101 + sin(.45)*5/2*sin(.45)", 0b101001111L + 0b0110000101L + Math.sin(.45) * 5L / 2* Math.sin(.45)},
                {"cos(3.14/2)", Math.cos(3.14/2d)}
        };
        return Arrays.asList(data);
    }

    @Test
    public void testCalculate() {
        Assert.assertEquals(result, service.calculate(str));

    }
}
