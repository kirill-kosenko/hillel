package hillel.kosenko.kirill.calculation.service;

public interface Calculator<R, E> {
    R calculate(E expression);
}
