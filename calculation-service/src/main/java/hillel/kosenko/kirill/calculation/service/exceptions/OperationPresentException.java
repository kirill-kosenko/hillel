package hillel.kosenko.kirill.calculation.service.exceptions;

public class OperationPresentException extends RuntimeException {
    public OperationPresentException(String message) {
        super(message);
    }
}
