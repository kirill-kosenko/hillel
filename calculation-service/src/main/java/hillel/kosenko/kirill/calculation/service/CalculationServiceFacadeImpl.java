package hillel.kosenko.kirill.calculation.service;

import hillel.kosenko.kirill.calculation.service.operations.Operation;
import hillel.kosenko.kirill.calculation.service.operations.OperationManager;
import hillel.kosenko.kirill.calculation.service.operations.OperationManagerDefault;
import hillel.kosenko.kirill.calculation.service.parser.Parser;
import hillel.kosenko.kirill.calculation.service.parser.ParserFactory;
import hillel.kosenko.kirill.calculation.service.parser.SimpleParserFactory;

public class CalculationServiceFacadeImpl implements CalculationServiceFacade {

    private Parser parser;

    private Calculator<Number, Expression> calculator;

    private ParserFactory parserFactory;

    private OperationManager operationManager;

    public CalculationServiceFacadeImpl() {
        operationManager = new OperationManagerDefault();
        parserFactory = new SimpleParserFactory(operationManager);
        calculator = new CalculatorDefault(operationManager);
    }

    public CalculationServiceFacadeImpl(String parserName) {
        this();
        parser = parserFactory.getParser(parserName);
    }

    @Override
    public Number calculate(String str) {
        Expression e = parser.parse(str);
        return calculator.calculate(e);
    }

    @Override
    public String getParserName() {
        return parser.getName();
    }

    @Override
    public void changeParser(String parser) {
        this.parser = parserFactory.getParser(parser);
    }

    @Override
    public void add(String operator, Integer priority, Operation<Double> dop, Operation<Long> lop) {
        operationManager.add(operator, priority, dop, lop);
    }
}
