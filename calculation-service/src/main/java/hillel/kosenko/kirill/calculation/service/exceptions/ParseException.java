package hillel.kosenko.kirill.calculation.service.exceptions;

public class ParseException extends RuntimeException {
    public ParseException(String message) {
        super(message);
    }
}
