package hillel.kosenko.kirill.calculation.service.tokenizer;

public class Token {
    private TokenType tokenType;
    private String string;
    private Number number;

    public Token(TokenType tt, String string) {
        this.tokenType = tt;
        this.string = string;
        number = (tt == TokenType.POINT_NUMBER) ? Double.valueOf(string) :
                tt == TokenType.NUMBER ? Long.valueOf(string) :
                        tt == TokenType.BINARY_NUMBER ? Long.parseLong(string, 2) : number;
    }

    public Token(TokenType tt, Number number) {
        this.tokenType = tt;
        this.number = number;
    }

    public Token(TokenType tt, String string, Number number) {
        this.tokenType = tt;
        this.string = string;
        this.number = number;
    }

    public TokenType getType() {
        return tokenType;
    }

    public void setType(TokenType tokenType) {
        this.tokenType = tokenType;
    }

    public String getString() {
        return string;
    }

    public Number getNumber() {
        return number;
    }

    public void setNumber(Number number) {
        this.number = number;
    }

    public void setString(String string) {
        this.string = string;
    }

    public boolean isNumber() {
        return tokenType == TokenType.NUMBER ||
                tokenType == TokenType.POINT_NUMBER ||
                tokenType == TokenType.BINARY_NUMBER;
    }

    public boolean isOperator() {
        return tokenType == TokenType.OPERATOR;
    }

    public boolean isLeftBrace() {
        return tokenType == TokenType.LEFT_BRACE;
    }

    public boolean isRightBrace() {
        return tokenType == TokenType.RIGHT_BRACE;
    }

    public boolean isNotEOL() {
        return tokenType != TokenType.EOL;
    }

    @Override
    public String toString() {
        return "string='" + string;
    }
}
