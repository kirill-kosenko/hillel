package hillel.kosenko.kirill.calculation.service.tokenizer;

public enum TokenType {
    NUMBER, POINT_NUMBER, BINARY_NUMBER, OPERATOR, LEFT_BRACE, RIGHT_BRACE, EOL
}
