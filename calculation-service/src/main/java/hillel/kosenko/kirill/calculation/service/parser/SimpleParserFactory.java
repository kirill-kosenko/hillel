package hillel.kosenko.kirill.calculation.service.parser;

import hillel.kosenko.kirill.calculation.service.operations.OperationManager;

import java.util.HashMap;
import java.util.Map;

public class SimpleParserFactory implements ParserFactory {

    public static final String RUTISHAUSER = "Rutishauser";
    public static final String BAUER_ZAMELZON = "BauerZamelzon";
    public static final String RECURSIVE_DESCENT = "RecursiveDescent";

    private OperationManager operationManager;

    private Map<String, Parser> instances = new HashMap<>(3);

    public SimpleParserFactory(OperationManager operationManager) {
        this.operationManager = operationManager;
    }

    @Override
    public Parser getParser() {
        return getParser(RECURSIVE_DESCENT);
    }

    @Override
    public Parser getParser(String str) {
        Parser parser = instances.get(str);
        return parser != null ? parser : instantiate(str);
    }

    private Parser instantiate(String str) {
        Parser parser = null;
        switch (str) {
            case RUTISHAUSER:
                parser = new RutishauserParser(operationManager.getBasicOperators());
                break;
            case BAUER_ZAMELZON:
                parser = new BauerZamelzonParser(operationManager.getAllOperatorsPriorities());
                break;
            case RECURSIVE_DESCENT:
                parser = new RecursiveDescentParser(operationManager.getAllOperatorsPriorities());
        }
        instances.put(str, parser);
        return parser;
    }
}
