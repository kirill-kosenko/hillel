package hillel.kosenko.kirill.calculation.service.tokenizer;

import java.util.Set;

public interface ArithmeticTokenizer<S, T> extends Tokenizer<S, T> {

    T next();

    void setOperators(Set<S> operations);

    T getNullNumberToken();

}
