package hillel.kosenko.kirill.calculation.service;

import hillel.kosenko.kirill.calculation.service.operations.Operation;

public interface CalculationServiceFacade {
    Number calculate(String str);
    void changeParser(String parser);
    String getParserName();
    void add(String operator, Integer priority, Operation<Double> dop, Operation<Long> lop);
}
