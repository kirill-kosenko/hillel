package hillel.kosenko.kirill.calculation.service.exceptions;

public class InvalidInputException extends RuntimeException {

    private final static String MESSAGE = "Invalid input string";

    public InvalidInputException() {
        super(MESSAGE);
    }

    public InvalidInputException(String message) {
        super(message);
    }
}
