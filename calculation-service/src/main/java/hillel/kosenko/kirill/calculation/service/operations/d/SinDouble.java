package hillel.kosenko.kirill.calculation.service.operations.d;

import hillel.kosenko.kirill.calculation.service.operations.Operation;

public class SinDouble implements Operation<Double> {
    @Override
    public Double calculate(Double op1, Double op2) {
        return Math.sin(op2);
    }
}
