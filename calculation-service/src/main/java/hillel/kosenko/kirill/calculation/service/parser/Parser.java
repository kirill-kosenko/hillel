package hillel.kosenko.kirill.calculation.service.parser;

import hillel.kosenko.kirill.calculation.service.Expression;

public interface Parser {
    Expression parse(String string);
    String getName();
}
