package hillel.kosenko.kirill.calculation.service.operations.d;


import hillel.kosenko.kirill.calculation.service.operations.Operation;

public class PowerDouble implements Operation<Double> {

    @Override
    public Double calculate(Double value, Double pow) {
        return Math.pow(value, pow);
    }
}
