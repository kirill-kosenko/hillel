package hillel.kosenko.kirill.calculation.service;

import hillel.kosenko.kirill.calculation.service.tokenizer.Token;

public class Expression {
    private Token token;
    private Expression[] args;

    public Expression(Token token) {
        this.token = token;
    }

    public Expression(Token token, Expression unary) {
        this.token = token;
        this.args = new Expression[] { unary };
    }

    public Expression(Token token, Expression left, Expression right) {
        this.token = token;
        this.args = new Expression[] { left, right };
    }

    public Token getToken() {
        return token;
    }

    public void setToken(Token token) {
        this.token = token;
    }

    public Expression[] getArgs() {
        return args;
    }

    public void setArgs(Expression[] args) {
        this.args = args;
    }

    public int argsSize() {
        if (args == null) return 0;
        return args.length;
    }
}
