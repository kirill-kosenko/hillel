package hillel.kosenko.kirill.calculation.service.tokenizer;

import java.util.Iterator;

public interface Tokenizer<S, T> extends Iterable<T> {

    void shiftLeftPos(int count);

    void set(S source);

    Iterator<T> iterator();
}
