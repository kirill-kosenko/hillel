package hillel.kosenko.kirill.calculation.service;

import hillel.kosenko.kirill.calculation.service.operations.Operation;
import hillel.kosenko.kirill.calculation.service.operations.OperationManager;

public class CalculatorDefault implements Calculator<Number, Expression> {

    private OperationManager operationManager;

    public CalculatorDefault(OperationManager operationManager) {
        this.operationManager = operationManager;
    }

    @Override
    public Number calculate(Expression expression) {
        switch (expression.argsSize()) {
            case 2:
                return calculateBinary(expression);
            case 1:
                return calculateUnary(expression);
            case 0:
                return getNumber(expression);
        }
        throw new IllegalArgumentException();
    }

    private Number calculateBinary(Expression e) {
        Number a = calculate(e.getArgs()[0]);
        Number b = calculate(e.getArgs()[1]);

        String operator = e.getToken().getString();

        return calculate(a, operator, b);
    }

    private Number calculateUnary(Expression e) {
        Number b = calculate(e.getArgs()[0]);
        String op = e.getToken().getString();

        return calculate(0L, op, b);
    }

    private Number getNumber(Expression e) {
        return e.getToken().getNumber();
    }

    private Number calculate(Number a, String op, Number b) {
        Number result;

        if (a instanceof Long && b instanceof Long) {
            result = calculateLong(a, op, b);
        } else {
            result = calculateDouble(a, op, b);
        }
        return result;
    }

    private Number calculateDouble(Number a, String op, Number b) {
        Operation<Double> dop = operationManager.getDouble(op);
        if (dop == null)
            return null;
        return dop.calculate(a.doubleValue(), b.doubleValue());
    }

    private Number calculateLong(Number a, String op, Number b) {
        Operation<Long> lop = operationManager.getLong(op);
        if (lop == null) {
            return calculateDouble(a, op, b);
        }
        return lop.calculate(a.longValue(), b.longValue());
    }
}
