package hillel.kosenko.kirill.calculation.service.parser;

import hillel.kosenko.kirill.calculation.service.Expression;
import hillel.kosenko.kirill.calculation.service.exceptions.InvalidInputException;
import hillel.kosenko.kirill.calculation.service.exceptions.ParseException;
import hillel.kosenko.kirill.calculation.service.tokenizer.ArithmeticTokenizer;
import hillel.kosenko.kirill.calculation.service.tokenizer.ArithmeticTokenizerImpl;
import hillel.kosenko.kirill.calculation.service.tokenizer.Token;

import java.util.Map;

public class RecursiveDescentParser implements Parser {

    private static final String NAME = "RecursiveDescent";

    private ArithmeticTokenizer<String, Token> tokenizer;
    private Map<String, Integer> supportedOperatorsPriorities;

    public RecursiveDescentParser(Map<String, Integer> supportedOperatorsPriorities) {
        this.supportedOperatorsPriorities = supportedOperatorsPriorities;
        tokenizer = ArithmeticTokenizerImpl.getInstance(supportedOperatorsPriorities.keySet());
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Expression parse(String str) {
        tokenizer.set(str);
        return _parse();
    }

    private Expression _parse() {
        return parseBinaryExpression(0);
    }

    private Expression parseSimpleExpression() {

        Token token = tokenizer.next();

        if (token.isLeftBrace()) {
            Expression res = _parse();
            if (!tokenizer.next().isRightBrace())
                throw new ParseException(") was expected");
            return res;
        }
        if (token.isNumber()) {
            return new Expression(token);
        }
        if (token.isOperator()) {
            return new Expression(token, parseSimpleExpression());
        }
        throw new InvalidInputException();
    }

    private Expression parseBinaryExpression(int minPriority) {
        Expression left = parseSimpleExpression();

        while (true) {
            Token operator = tokenizer.next();

            int priority = getPriority(operator.getString());
            if (priority <= minPriority) {
                tokenizer.shiftLeftPos(operator.getString().length());
                return left;
            }
            Expression right = parseBinaryExpression(priority);
            left = new Expression(operator, left, right);
        }
    }


    private int getPriority(String op) {
        Integer priority = supportedOperatorsPriorities.get(op);
        return priority != null ? priority : 0;
    }


}
