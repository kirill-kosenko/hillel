package hillel.kosenko.kirill.calculation.service.operations.l;

import hillel.kosenko.kirill.calculation.service.operations.Operation;

public class ModLong implements Operation<Long> {
    @Override
    public Long calculate(Long op1, Long op2) {
        return op1 % op2;
    }
}
