package hillel.kosenko.kirill.calculation.service.parser;

public interface ParserFactory {
    Parser getParser();
    Parser getParser(String name);
}
