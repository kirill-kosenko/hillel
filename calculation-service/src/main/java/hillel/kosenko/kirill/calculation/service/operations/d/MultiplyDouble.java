package hillel.kosenko.kirill.calculation.service.operations.d;


import hillel.kosenko.kirill.calculation.service.operations.Operation;

public class MultiplyDouble implements Operation<Double> {

    @Override
    public Double calculate(Double op1, Double op2) {
        return op1 * op2;
    }
}
