package hillel.kosenko.kirill.calculation.service.operations;

import hillel.kosenko.kirill.calculation.service.exceptions.OperationPresentException;
import hillel.kosenko.kirill.calculation.service.operations.d.*;
import hillel.kosenko.kirill.calculation.service.operations.l.*;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class OperationManagerDefault implements OperationManager {

    private static final String PLUS = "+";

    private static final String MINUS = "-";

    private static final String MUL = "*";

    private static final String DIV = "/";

    private static final String MOD = "%";

    private static final String POW = "**";

    private static final String AND = "&";

    private static final String OR = "|";

    private static final String XOR = "^";

    private static final String NOT = "~";

    private static final String SIN = "sin";

    private static final String COS = "cos";

    private static final int DEFAULT_PRIORITY = 100;

    private Map<String, Integer> basicOperatorsPriorities;

    private Map<String, Integer> allOperatorsPriorities;

    private Map<String, Operation<Double>> doubleOperations;

    private Map<String, Operation<Long>> longOperations;


    @Override
    public Operation<Double> getDouble(String operator) {
        return doubleOperations.get(operator);

    }

    @Override
    public Operation<Long> getLong(String operator) {
        return longOperations.get(operator);
    }


    private void addDouble(String operator, Operation<Double> operation) {
        if (checkNull(operator, operation)) return;

        checkIfPresent(operator, doubleOperations);
        doubleOperations.put(operator, operation);
    }


    private void addLong(String operator, Operation<Long> operation) {
        if (checkNull(operator, operation)) return;
        checkIfPresent(operator, longOperations);
        longOperations.put(operator, operation);
    }


    @Override
    public void add(String operator, Integer priority, Operation<Double> dop, Operation<Long> lop) {
        addDouble(operator, dop);
        addLong(operator, lop);
        if (priority == null)
            priority = DEFAULT_PRIORITY;
        allOperatorsPriorities.put(operator, priority);
    }

    @Override
    public Set<String> getBasicOperators() {
        return basicOperatorsPriorities.keySet();
    }

    @Override
    public Map<String, Integer> getAllOperatorsPriorities() {
        return allOperatorsPriorities;
    }

    @Override
    public Set<String> getAllOperators() {
        return allOperatorsPriorities.keySet();
    }


    private void checkIfPresent(String op, Map operations) {
        if (operations.get(op) != null)
            throw new OperationPresentException("Operation \"" + op + "\" is present");
    }

    private boolean checkNull(String operator, Operation<? extends Number> operation) {
        if (operator == null) throw new NullPointerException("operator is null");
        return operation == null;
    }

    {
        basicOperatorsPriorities = new LinkedHashMap<String, Integer>() {
            {
                put(OR, 10);
                put(XOR, 20);
                put(AND, 30);
                put(PLUS, 40);
                put(MINUS, 40);
                put(MUL, 50);
                put(DIV, 50);
                put(MOD, 50);
                put(POW, 60);
                put(NOT, 70);
            }
        };
        allOperatorsPriorities = new LinkedHashMap<String, Integer>(basicOperatorsPriorities) {
            {
                put(COS, 100);
                put(SIN, 100);
            }
        };

        doubleOperations =
                new HashMap<String, Operation<Double>>() {{
                    put(PLUS, new AdditionDouble());
                    put(MINUS, new SubtractionDouble());
                    put(MUL, new MultiplyDouble());
                    put(DIV, new DivisionDouble());
                    put(POW, new PowerDouble());
                    put(MOD, new ModDouble());
                    put(SIN, new SinDouble());
                    put(COS, new CosDouble());
                }};

        longOperations =
                new HashMap<String, Operation<Long>>() {{
                    put(PLUS, new AdditionLong());
                    put(MINUS, new SubtractionLong());
                    put(MUL, new MultiplyLong());
                    put(MOD, new ModLong());
                    put(AND, new AndLong());
                    put(OR, new OrLong());
                    put(XOR, new XorLong());
                    put(NOT, new NotLong());
                }};
    }

}
