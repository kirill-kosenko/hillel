package hillel.kosenko.kirill.calculation.service.operations;


public interface Operation<T extends Number> {
    T calculate(T op1, T op2);
}
