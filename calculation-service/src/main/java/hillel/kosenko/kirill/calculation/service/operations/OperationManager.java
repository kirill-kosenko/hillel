package hillel.kosenko.kirill.calculation.service.operations;

import java.util.Map;
import java.util.Set;

public interface OperationManager {
    Operation<Double> getDouble(String operator);

    Operation<Long> getLong(String operator);

    void add(String operator, Integer priority,
             Operation<Double> dop, Operation<Long> lop);

    Map<String, Integer> getAllOperatorsPriorities();

    Set<String> getBasicOperators();

    Set<String> getAllOperators();
}
