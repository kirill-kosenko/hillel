package hillel.kosenko.kirill.calculation.service.parser;

import hillel.kosenko.kirill.calculation.service.Expression;
import hillel.kosenko.kirill.calculation.service.exceptions.InvalidInputException;
import hillel.kosenko.kirill.calculation.service.tokenizer.ArithmeticTokenizer;
import hillel.kosenko.kirill.calculation.service.tokenizer.ArithmeticTokenizerImpl;
import hillel.kosenko.kirill.calculation.service.tokenizer.Token;

import java.util.*;

public class BauerZamelzonParser implements Parser {

    private static final String NAME = "BauerZamelzon";

    private static final String LEFT_BRACE = "(";
    private static final String RIGHT_BRACE = ")";
    private static final String EMPTY = "$";

    private static final int F1 = 1;
    private static final int F2 = 2;
    private static final int F3 = 3;
    private static final int F4 = 4;
    private static final int F5 = 5;
    private static final int F6 = 6;

    private Deque<Expression> operandsStack = new LinkedList<>();
    private Deque<Token> operatorsStack = new LinkedList<>();

    private ArithmeticTokenizer<String, Token> tokenizer;

    private Token prevToken;

    private List<String> supportedOperators;

    private int[][] functionsTable;

    public BauerZamelzonParser(Map<String, Integer> supportedOperatorsPriorities) {

        this.tokenizer = ArithmeticTokenizerImpl.getInstance(
                supportedOperatorsPriorities.keySet());

        supportedOperators = new ArrayList<String>() {{
            add(EMPTY);
            add(LEFT_BRACE);
            addAll(supportedOperatorsPriorities.keySet());
            add(RIGHT_BRACE);
        }};

        functionsTable = MatrixCreator.create(supportedOperatorsPriorities, supportedOperators);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Expression parse(String str) {
        tokenizer.set(LEFT_BRACE + str + RIGHT_BRACE);
        return _parse();
    }

    private Expression _parse() {
        Iterator<Token> iterator = tokenizer.iterator();
        while (iterator.hasNext()) {
            Token token = iterator.next();
            token = checkUnary(token);

            if (token.isNumber()) {
                pushOperand(token);
            } else {
                executeFunction(token);
            }
            prevToken = token;
        }
        return operandsStack.pop();
    }

    private Token checkUnary(Token token) {
        if (prevToken != null && !prevToken.isNumber() && !prevToken.isRightBrace() && (token.isOperator())) {
            pushOperator(token);
            token = tokenizer.getNullNumberToken();
        }
        return token;
    }

    private void pushOperand(Token op) {
        operandsStack.push(new Expression(op));
    }

    private void pushOperand(Expression expression) {
        operandsStack.push(expression);
    }

    private void pushOperator(Token operator) {
        operatorsStack.push(operator);
    }

    private Expression createExpression() {
        Expression b = operandsStack.pop();
        Expression a = operandsStack.pop();
        Token op = operatorsStack.pop();
        return new Expression(op, a, b);
    }

    private void executeFunction(Token token) {
        int n = determineFunctionNumber(token);
        executeFunction(n, token);
    }

    private int determineFunctionNumber(Token token) {
        int strIndex = supportedOperators.indexOf(token.getString());
        if (strIndex == -1)
            f5();
        Token op = operatorsStack.peek();
        int stackIndex = 0;
        if (op != null)
            stackIndex = supportedOperators.indexOf(op.getString());
        return functionsTable[stackIndex][strIndex];
    }

    private void f1(Token token) {
        pushOperator(token);
    }


    private void f2(Token operator) {
        Expression result = createExpression();

        pushOperand(result);
        pushOperator(operator);
    }

    private void f3() {
        operatorsStack.removeFirst();
    }

    private void f4(Token token) {
        Expression result = createExpression();
        pushOperand(result);
        executeFunction(token);
    }

    private void f5() {
        throw new InvalidInputException();
    }

    private void executeFunction(int number, Token tokenStr) {
        switch (number) {
            case F1:
                f1(tokenStr);
                break;
            case F2:
                f2(tokenStr);
                break;
            case F3:
                f3();
                break;
            case F4:
                f4(tokenStr);
                break;
            case F5:
                f5();
        }
    }

    private static class MatrixCreator {

        static int[][] create(Map<String, Integer> operatorsPriorities, List<String> operators) {
            int[][] matrix = new int[operators.size() - 1][operators.size()];

            for (int i = 0; i < operators.size(); i++) {

                String stackOp = operators.get(i);
                if (stackOp.equals(RIGHT_BRACE)) continue;

                int[] line = new int[operators.size()];
                for (int j = 0; j < operators.size(); j++) {
                    String inputOp = operators.get(j);
                    if (stackOp.equals(EMPTY)) {
                        if (inputOp.equals(EMPTY)) {
                            line[j] = F6;
                        } else if (inputOp.equals(RIGHT_BRACE)) {
                            line[j] = F5;
                        } else {
                            line[j] = F1;
                        }
                    } else if (stackOp.equals(LEFT_BRACE)) {
                        if (inputOp.equals(EMPTY)) {
                            line[j] = F5;
                        } else if (inputOp.equals(RIGHT_BRACE)) {
                            line[j] = F3;
                        } else {
                            line[j] = F1;
                        }
                    } else {
                        if (inputOp.equals(EMPTY) || inputOp.equals(RIGHT_BRACE)) {
                            line[j] = F4;
                        } else if (inputOp.equals(LEFT_BRACE)) {
                            line[j] = F1;
                        } else {
                            int sopPriority = operatorsPriorities.get(stackOp);
                            int iopPriority = operatorsPriorities.get(inputOp);
                            if (sopPriority == iopPriority) {
                                line[j] = F2;
                            } else if (sopPriority < iopPriority) {
                                line[j] = F1;
                            } else {
                                line[j] = F4;
                            }
                        }
                    }
                }
                matrix[i] = line;
            }
            return matrix;
        }
    }
}