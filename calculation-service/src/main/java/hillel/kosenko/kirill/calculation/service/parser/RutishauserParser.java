package hillel.kosenko.kirill.calculation.service.parser;

import hillel.kosenko.kirill.calculation.service.Expression;
import hillel.kosenko.kirill.calculation.service.exceptions.ParseException;
import hillel.kosenko.kirill.calculation.service.tokenizer.ArithmeticTokenizer;
import hillel.kosenko.kirill.calculation.service.tokenizer.ArithmeticTokenizerImpl;
import hillel.kosenko.kirill.calculation.service.tokenizer.Token;

import java.util.*;

public class RutishauserParser implements Parser {

    private static final String NAME = "Rutishauser";

    private ArithmeticTokenizer<String, Token> tokenizer;

    private Token prevToken;
    private int level;

    private List<TokenLevel> tokenLevels;


    public RutishauserParser(Set<String> supportedOperators) {
        tokenizer = ArithmeticTokenizerImpl.getInstance(supportedOperators);
    }

    public String getName() {
        return NAME;
    }

    @Override
    public Expression parse(String str) {
        initialize(str);
        defineTokenLevels(str);
        return createExpressionTree();
    }

    private void initialize(String input) {
        tokenizer.set(input);
        tokenLevels = new ArrayList<>(input.length());
    }

    private void defineTokenLevels(String str) {
        Iterator<Token> iterator = tokenizer.iterator();
        while (iterator.hasNext()) {
            Token token = iterator.next();
            checkIfUnary(token);
            addToken(token);

            prevToken = token;
        }
    }

    private void checkIfUnary(Token token) {
        if (prevToken != null && prevToken.isLeftBrace() && token.isOperator()) {
            Token nullToken = tokenizer.getNullNumberToken();
            addToken(nullToken);
        }
    }

    private void addToken(Token token) {
        changeLevelFor(token);
        addToList(token);
    }

    private void addToList(Token token) {
        tokenLevels.add(new TokenLevel(level, token));
    }

    private void changeLevelFor(Token token) {
        if (token.isLeftBrace() || token.isNumber()) {
            level++;
        } else if (token.isOperator() || token.isRightBrace()) {
            level--;
        }
    }

    private TokenLevel maxTokenLevel() {
        return Collections.max(tokenLevels);
    }

    private Expression createExpressionTree() {
        Expression expression = null;
        TokenLevel tl;
        while (tokenLevels.size() != 1) {
            tl = maxTokenLevel();
            expression = createExpression(tl);
            deleteBracketsAndSaveResult(getIndex(tl), expression);
        }
        return expression;
    }

    private Expression createExpression(TokenLevel tl) {
        int index = getIndex(tl);
        Expression op1 = createSimpleExpression(index);
        Expression op2 = createSimpleExpression(getNextOperandIndex(index + 1));
        Token operator = (Token) tokenLevels.get(getNextOperatorIndex(index + 1)).getToken();
        return new Expression(operator, op1, op2);
    }

    private Expression createSimpleExpression(int index) {
        Object op = tokenLevels.get(index).token;
        if (op instanceof Expression) return (Expression) op;

        return new Expression((Token) op);
    }

    private void deleteBracketsAndSaveResult(int index, Object result) {
        tokenLevels.subList(index, getRightBraceIndex(index + 1) + 1).clear();
        tokenLevels.get(getLeftBraceIndex(index - 1)).setToken(result);
    }

    private int getIndex(TokenLevel tl) {
        return tokenLevels.indexOf(tl);
    }

    private int getNextOperandIndex(int from) {
        for (int i = from; i < tokenLevels.size(); i++) {
            Object token = tokenLevels.get(i).token;
            if (token instanceof Token && ((Token) token).isNumber()
                    || token instanceof Expression) {
                return i;
            }
        }
        throw new ParseException("Number was expected");
    }

    private int getNextOperatorIndex(int from) {
        for (int i = from; i < tokenLevels.size(); i++) {
            Object token = tokenLevels.get(i).token;
            if (token instanceof Token && ((Token) token).isOperator()) {
                return i;
            }
        }
        throw new ParseException("Operator was expected");
    }

    private int getRightBraceIndex(int from) {
        for (int i = from; i < tokenLevels.size(); i++) {
            Object token = tokenLevels.get(i).token;
            if (token instanceof Token && ((Token) token).isRightBrace()) {
                return i;
            }
        }
        throw new ParseException(") was expected");
    }

    private int getLeftBraceIndex(int from) {
        for (int i = from; i >= 0; i--) {
            Object token = tokenLevels.get(i).token;
            if (token instanceof Token && ((Token) token).isLeftBrace()) {
                return i;
            }
        }
        throw new ParseException("( was expected");
    }

    private static class TokenLevel implements Comparable<TokenLevel> {

        private Integer level;
        private Object token;

        public TokenLevel(Integer level, Object token) {
            this.level = level;
            this.token = token;
        }

        @Override
        public int compareTo(TokenLevel t) {
            return level.compareTo(t.getLevel());
        }

        public Integer getLevel() {
            return level;
        }

        public void setLevel(Integer level) {
            this.level = level;
        }

        public Object getToken() {
            return token;
        }

        public void setToken(Object token) {
            this.token = token;
        }

        @Override
        public String toString() {
            return "level=" + level +
                    ", token=" + token;
        }
    }
}
