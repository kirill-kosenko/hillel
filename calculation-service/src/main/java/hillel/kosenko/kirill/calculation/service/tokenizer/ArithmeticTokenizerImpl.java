package hillel.kosenko.kirill.calculation.service.tokenizer;

import hillel.kosenko.kirill.calculation.service.exceptions.InvalidInputException;

import java.util.Iterator;
import java.util.Set;

public class ArithmeticTokenizerImpl implements ArithmeticTokenizer<String, Token> {

    private static final String _0B = "0b";

    private static final Character LEFT_BRACE = '(';

    private static final Character RIGHT_BRACE = ')';

    private static final String EMPTY_STRING = "";

    private static final char ONE = '1';

    private static final char ZERO = '0';

    private static final Character POINT = '.';

    private static final int START_POS = 0;

    private String s;

    private int pos;

    private Token current;

    private Set<String> allOperators;

    private static ArithmeticTokenizerImpl tokenizer;

    private ArithmeticTokenizerImpl() {
    }

    private ArithmeticTokenizerImpl(Set<String> allOperators) {
        this.allOperators = allOperators;
    }

    public static ArithmeticTokenizerImpl getInstance(Set<String> operators) {
        if (tokenizer == null) {
            return new ArithmeticTokenizerImpl(operators);
        }
        tokenizer.setOperators(operators);
        return tokenizer;
    }

    @Override
    public Token next() {
        return getToken();
    }


    @Override
    public void set(String s) {
        this.s = s;
        pos = START_POS;
    }

    @Override
    public void setOperators(Set<String> operators) {
        this.allOperators = operators;
    }

    @Override
    public Token getNullNumberToken() {
        return new Token(TokenType.NUMBER, "0");
    }

    @Override
    public void shiftLeftPos(int count) {
        pos -= count;
    }

    @Override
    public Iterator<Token> iterator() {
        return new IteratorImpl();
    }

    protected Token getToken() {
        readSpaces();

        if (pos == s.length())
            return new Token(TokenType.EOL, EMPTY_STRING);
        if (isBinary()) {
            pos += _0B.length();
            return readBinary();
        }
        if (isDigitOrPoint(s.charAt(pos))) {
            return readDecimal();
        }
        if (s.charAt(pos) == LEFT_BRACE)
            return readLeftBrace();
        if (s.charAt(pos) == RIGHT_BRACE)
            return readRightBrace();

        Token token = readOperator();
        if (token.isOperator())
            return token;

        throw new InvalidInputException();

    }

    private Token readOperator() {

        String sub = s.substring(pos);
        String res = EMPTY_STRING;
        TokenType tt = null;

        for (String operator : allOperators) {
            if (sub.startsWith(operator)) {
                pos += operator.length();
                res = operator;
                tt = TokenType.OPERATOR;
                break;
            }
        }

        return new Token(tt, res);
    }

    private void readSpaces() {
        while (pos < s.length() && Character.isWhitespace(s.charAt(pos))) {
            pos++;
        }
    }

    private Token readDecimal() {
        String res = EMPTY_STRING;
        TokenType tt = TokenType.NUMBER;
        while (pos < s.length() && isDigitOrPoint(s.charAt(pos))) {
            if (isPoint(s.charAt(pos)))
                tt = TokenType.POINT_NUMBER;
            res += s.charAt(pos++);
        }
        return new Token(tt, res);
    }

    private Token readBinary() {
        String res = EMPTY_STRING;
        while (pos < s.length() && Character.isDigit(s.charAt(pos))) {
            if (isZeroOrOne(s.charAt(pos)))
                res += s.charAt(pos++);
            else throw new InvalidInputException();
        }
        return new Token(TokenType.BINARY_NUMBER, res);
    }

    private Token readLeftBrace() {
        return new Token(TokenType.LEFT_BRACE, getBrace());
    }

    private Token readRightBrace() {
        return new Token(TokenType.RIGHT_BRACE, getBrace());
    }

    private String getBrace() {
        return Character.toString(s.charAt(pos++));
    }

    private boolean isZeroOrOne(char c) {
        return c == ZERO || c == ONE;
    }

    private boolean isBinary() {
        return s.substring(pos).startsWith(_0B);
    }

    protected boolean isDigitOrPoint(char c) {
        return Character.isDigit(c) || isPoint(c);
    }

    protected boolean isPoint(char c) {
        return c == POINT;
    }

    private class IteratorImpl implements Iterator<Token> {
        private Token current;

        @Override
        public boolean hasNext() {
            return (current = getToken()).getType() != TokenType.EOL;
        }

        @Override
        public Token next() {
            return current;
        }
    }
}
