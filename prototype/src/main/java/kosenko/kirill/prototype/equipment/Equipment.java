package kosenko.kirill.prototype.equipment;

public abstract class Equipment implements Cloneable {
    @Override
    public Equipment clone() throws CloneNotSupportedException {
        return (Equipment) super.clone();
    }
}
