package kosenko.kirill.prototype;

import kosenko.kirill.prototype.equipment.*;

import java.util.ArrayList;
import java.util.List;

public class ComfortCar extends Car {

    private static final int SEATS = 4;
    private static final int DOORS = 4;
    private static final int WHEELS = 4;

    protected List<Equipment> equipments;

    public ComfortCar() {
        super(DOORS, WHEELS, SEATS, Color.WHITE);
        equipments = new ArrayList<Equipment>() {
            {
                add(new ABS());
                add(new AirConditioning());
                add(new CentralDoorLocking());
                add(new DriverSideAirbag());
                add(new ElectricalSideMirrors());
                add(new Immobilizer());
                add(new OnBoardComputer());
                add(new Radio());
            }
        };
    }


    public List<Equipment> getEquipments() {
        return equipments;
    }

    public void setEquipments(List<Equipment> equipments) {
        this.equipments = equipments;
    }

    @Override
    public Car clone() throws CloneNotSupportedException {
        Car car = super.clone();
        car.setEquipments(cloneEquipments());
        return car;
    }

    protected List<Equipment> cloneEquipments() throws CloneNotSupportedException {
        List<Equipment> target = new ArrayList<>(equipments.size());
        for (Equipment e : equipments) {
            target.add(e.clone());
        }
        return target;
    }
}
