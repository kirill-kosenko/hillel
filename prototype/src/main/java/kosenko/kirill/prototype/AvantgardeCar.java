package kosenko.kirill.prototype;

import kosenko.kirill.prototype.equipment.*;

public class AvantgardeCar extends ComfortCar {

    public AvantgardeCar() {
        addEquipment(new AlloyWheels());
        addEquipment(new AutomaticClimateControl());
        addEquipment(new CruiseControl());
        addEquipment(new ElectricallyAdjustableSeats());
        addEquipment(new ElectronicStabilityControl());
        addEquipment(new HandsFree());
        addEquipment(new NavigationSystem());
        addEquipment(new ParkDistanceControl());
        addEquipment(new PassengerSideAirbag());
        addEquipment(new RainSensor());
        addEquipment(new TractionControl());
        addEquipment(new XenonHeadlights());
    }

    @Override
    public Car clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
