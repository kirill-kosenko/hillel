package kosenko.kirill.prototype;

import kosenko.kirill.prototype.equipment.Equipment;

import java.util.List;

public abstract class Car implements Cloneable {
    private String number;
    private int doors;
    private int wheels;
    private int seats;
    private int engine;


    private Color color;

    public Car(int doors, int wheels, int seats, Color color) {
        this.doors = doors;
        this.wheels = wheels;
        this.seats = seats;
        this.color = color;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getDoors() {
        return doors;
    }

    public void setDoors(int doors) {
        this.doors = doors;
    }

    public int getWheels() {
        return wheels;
    }

    public void setWheels(int wheels) {
        this.wheels = wheels;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void installEngine(int engine) {
        if (engine <= 0) return;
        this.engine = engine;
    }

    protected void addEquipment(Equipment equipment) {
        getEquipments().add(equipment);
    }

    @Override
    protected Car clone() throws CloneNotSupportedException {
        return (Car) super.clone();
    }

    protected abstract List<Equipment> getEquipments();
    protected abstract void setEquipments(List<Equipment> equipments);

}
