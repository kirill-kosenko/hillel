package kosenko.kirill.prototype;

import java.util.HashMap;
import java.util.Map;

public class CarFactory {

    public  static final String COMFORT = "comfort";
    public  static final String AVANTGARDE = "avantgarde";

    private static Map<String, Car> prototypes = new HashMap<>();

    static {
        prototypes.put(COMFORT, new ComfortCar());
        prototypes.put(AVANTGARDE, new AvantgardeCar());
    }

    public static Car getInstance(String s) throws CloneNotSupportedException {
        return prototypes.get(s).clone();
    }
}
