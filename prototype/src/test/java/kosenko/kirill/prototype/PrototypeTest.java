package kosenko.kirill.prototype;

import org.junit.Assert;
import org.junit.Test;

public class PrototypeTest {

    @Test
    public void comfortTest() throws CloneNotSupportedException {
        Car comfort180 = CarFactory.getInstance(CarFactory.COMFORT);
        comfort180.setNumber("AE3212AE");
        comfort180.installEngine(180);

        Car comfort200 = CarFactory.getInstance(CarFactory.COMFORT);
        comfort200.setNumber("AE1231CM");
        comfort200.installEngine(200);

        Assert.assertNotSame(comfort180.getEquipments(), comfort200.getEquipments());
        Assert.assertEquals(comfort180.getDoors(), comfort200.getDoors());
        Assert.assertEquals(comfort180.getSeats(), comfort200.getSeats());
        Assert.assertEquals(comfort180.getWheels(), comfort200.getWheels());
    }

    @Test
    public void avantgardeTest() throws CloneNotSupportedException {
        Car avantgarde300 = CarFactory.getInstance(CarFactory.AVANTGARDE);
        avantgarde300.setNumber("II1211II");
        avantgarde300.installEngine(300);

        Car avantgarde430 = CarFactory.getInstance(CarFactory.AVANTGARDE);
        avantgarde430.setNumber("AI0001II");
        avantgarde430.installEngine(430);

        Assert.assertNotSame(avantgarde300.getEquipments(), avantgarde430.getEquipments());
        Assert.assertEquals(avantgarde300.getDoors(), avantgarde430.getDoors());
        Assert.assertEquals(avantgarde300.getSeats(), avantgarde430.getSeats());
        Assert.assertEquals(avantgarde300.getWheels(), avantgarde430.getWheels());
    }

}
