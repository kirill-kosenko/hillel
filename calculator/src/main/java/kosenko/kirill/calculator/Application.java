package kosenko.kirill.calculator;

import hillel.kosenko.kirill.calculation.service.CalculationServiceFacadeImpl;
import kosenko.kirill.calculator.controller.CalculationController;
import kosenko.kirill.calculator.controller.Controller;
import kosenko.kirill.calculator.exceptions.QuitException;
import kosenko.kirill.calculator.input.ConsoleInput;
import kosenko.kirill.calculator.input.Input;
import kosenko.kirill.calculator.model.CalculationModel;
import kosenko.kirill.calculator.model.Model;
import kosenko.kirill.calculator.view.ConsoleView;
import kosenko.kirill.calculator.view.View;

import java.io.IOException;

public class Application {

    public static void main(String[] args) {
        Model model = new CalculationModel(
                new CalculationServiceFacadeImpl());
        View view = new ConsoleView();

        model.registerView(view);

        Input input = new ConsoleInput();
        CalculationController controller = new CalculationController(model);

        try {
            setParser(controller, input);
            while (true) {
                try {
                    String line = input.read();
                    controller.execute(line);
                } catch (QuitException e) {
                    input.closeResource();
                    exit();
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }


        } catch (Exception e) {
            System.out.println(e.getMessage());
        } catch (Throwable e) {
            try {
                input.closeResource();
            } catch (IOException io) {
                System.out.println(io.getMessage());
            }

            throw e;
        }
    }

    private static void setParser(Controller controller, Input input) throws IOException {
        String command = Command.PARSER_CHANGE;
        command += input.readParserName();
        controller.execute(command);
    }

    private static void exit() {
        System.exit(0);
    }
}
