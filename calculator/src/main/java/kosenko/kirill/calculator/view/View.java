package kosenko.kirill.calculator.view;

public interface View {
    void render(String result);
}
