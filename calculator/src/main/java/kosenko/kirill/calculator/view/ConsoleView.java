package kosenko.kirill.calculator.view;

public class ConsoleView implements View {
    @Override
    public void render(String result) {
        System.out.println(result);
    }
}
