package kosenko.kirill.calculator.model;

import hillel.kosenko.kirill.calculation.service.CalculationServiceFacade;
import kosenko.kirill.calculator.view.View;

import java.util.HashSet;
import java.util.Set;

public class CalculationModel implements Model {

    private static final String EQUAL = "=";

    private CalculationServiceFacade service;

    private Set<View> views;

    public CalculationModel(CalculationServiceFacade service) {
        this.service = service;
        views = new HashSet<>();
    }

    @Override
    public void calculate(String str) {
        Number result = service.calculate(str);
        String rendered = str + EQUAL + result;
        render(rendered);
    }

    @Override
    public void changeParser(String name) {
        service.changeParser(name);
    }

    @Override
    public void registerView(View view) {
        this.views.add(view);
    }

    @Override
    public void removeView(View view) {
        this.views.remove(view);
    }

    @Override
    public void render(String str) {
        this.views.forEach(v -> v.render(str));
    }
}
