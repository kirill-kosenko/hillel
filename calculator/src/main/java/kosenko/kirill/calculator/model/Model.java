package kosenko.kirill.calculator.model;

import kosenko.kirill.calculator.view.View;

public interface Model  {

    void calculate(String str);

    void changeParser(String name);

    void registerView(View view);

    void removeView(View view);

    void render(String str);
}
