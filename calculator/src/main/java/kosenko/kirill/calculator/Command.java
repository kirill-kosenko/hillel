package kosenko.kirill.calculator;

public interface Command {

    String QUIT = "q";

    String PARSER_CHANGE = "p";
}
