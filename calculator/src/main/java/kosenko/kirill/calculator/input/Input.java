package kosenko.kirill.calculator.input;

import java.io.IOException;

public interface Input {
    String read() throws IOException;
    String readParserName() throws IOException;
    void closeResource() throws IOException;
}
