package kosenko.kirill.calculator.input;

import hillel.kosenko.kirill.calculation.service.exceptions.InvalidInputException;
import kosenko.kirill.calculator.Command;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleInput implements Input {

    private static final String BAUER_ZAMELZON = "BauerZamelzon";

    private static final String RUTISHAUSER = "Rutishauser";

    private static final String RECURSIVE_DESCENT = "RecursiveDescent";

    private static final String PARSER_MENU =
            "1 - " + RUTISHAUSER + "\n2 - " + BAUER_ZAMELZON + "\n3 - " + RECURSIVE_DESCENT + "\nparser->";

    private static final String UNKNOWN_PARSER = "Unknown parser id";

    private static String currentParser;

    private BufferedReader br;

    public ConsoleInput() {
        br = new BufferedReader(new InputStreamReader(System.in));
    }

    @Override
    public String read() throws IOException {
        System.out.print(currentParser + "->");
        String line = br.readLine();
        if (Command.PARSER_CHANGE.equals(line)) {
            line += readParserName();
        }
        return line;
    }

    @Override
    public String readParserName() throws IOException {
        System.out.print(PARSER_MENU);
        int p = Integer.valueOf(br.readLine());
        String name = getParserName(p);
        currentParser = name;
        return name;
    }

    private String getParserName(int id) {
        switch (id) {
            case 1:
                return RUTISHAUSER;
            case 2:
                return BAUER_ZAMELZON;
            case 3:
                return RECURSIVE_DESCENT;
            default:
                throw new InvalidInputException(UNKNOWN_PARSER);
        }
    }

    @Override
    public void closeResource() throws IOException {
        if (br != null)
            br.close();
    }
}
