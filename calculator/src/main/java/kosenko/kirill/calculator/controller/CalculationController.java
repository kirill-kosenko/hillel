package kosenko.kirill.calculator.controller;

import kosenko.kirill.calculator.Command;
import kosenko.kirill.calculator.exceptions.QuitException;
import kosenko.kirill.calculator.model.Model;

public class CalculationController implements Controller {

    private Model model;

    public CalculationController(Model model) {
        this.model = model;
    }

    @Override
    public void execute(String str) {
        if (str.startsWith(Command.QUIT))
            throw new QuitException();
        if (str.startsWith(Command.PARSER_CHANGE)) {
            changeParser(str);
        } else {
            calculate(str);
        }
    }

    private void changeParser(String str) {
        String name = retrieveName(str);
        model.changeParser(name);
    }

    private void calculate(String str) {
        model.calculate(str);
    }

    private String retrieveName(String str) {
        int index = Command.PARSER_CHANGE.length();
        return str.substring(index);
    }
}
