package kosenko.kirill.calculator.controller;

public interface Controller {
    void execute(String str);
}
