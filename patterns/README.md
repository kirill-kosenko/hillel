## Three patterns have been implemented in `calculation-service` module:

#### -`Iterator`, `Singleton` - see [ArithmeticTokenizerImpl.java](../calculation-service/src/main/java/hillel/kosenko/kirill/calculation/service/tokenizer/ArithmeticTokenizerImpl.java)

#### -`Facade` - see [CalculationServiceFacadeImpl.java](../calculation-service/src/main/java/hillel/kosenko/kirill/calculation/service/CalculationServiceFacadeImpl.java)

## The last one `Observer/Observable` - in `calculator` module: 
#### -`Observable`- see [CalculationModel.java](../calculator/src/main/java/kosenko/kirill/calculator/model/CalculationModel.java)
#### -`Observer` - see [ConsoleView.java](../calculator/src/main/java/kosenko/kirill/calculator/view/ConsoleView.java)